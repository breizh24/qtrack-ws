const express = require('express')
const helmet = require('helmet')
const https = require('http')
const fs = require('fs')

function handleMessage(data){
  const parsedData = JSON.parse(data)
  const dataToSend = JSON.stringify({data: parsedData.data, socketId: parsedData.socket_id})
  return {room: parsedData.room, dataToSend}
}

const app = express()
app.use(helmet())

const port = process.env.PORT || 3000;
const options = {
  // in case we need to use the ssl: on line 3 change require('http') to require('https')
  // uncomment the 2 line below and set the certs path
  // key: fs.readFileSync("/etc/letsencrypt/live/socket.quickalgorithm.com/privkey.pem"),
  // cert: fs.readFileSync("/etc/letsencrypt/live/socket.quickalgorithm.com/fullchain.pem")
}
const server = https.createServer(options, app)

const io = require('socket.io')({
  serveClient: false,
});

io.attach(server, {
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
});

io.on('connection', (socket) => {
  socket.emit('connected', { id: socket.id });

  socket.on('room', room => {
    socket.join(room)
  })

  socket.on('notification', data => {
    const {room, dataToSend} = handleMessage(data)
    io.to(room).emit('notification', dataToSend)
  })

  socket.on('comment', data => {
    const {room, dataToSend} = handleMessage(data)
    io.to(room).emit('comment', dataToSend)
  })

  socket.on('event_template', data => {
    const {room, dataToSend} = handleMessage(data)
    io.to(room).emit('event_template', dataToSend)
  })

  socket.on('asset_update', data => {
    const {room, dataToSend} = handleMessage(data)
    io.to(room).emit('asset_update', dataToSend)
  })

  socket.on('asset_delete', data => {
    const {room, dataToSend} = handleMessage(data)
    io.to(room).emit('asset_delete', dataToSend)
  })

  socket.on('alert_update', data => {
    const {room, dataToSend} = handleMessage(data)
    io.to(room).emit('alert_update', dataToSend)
  })

  socket.on('tags_update', data => {
    const {room, dataToSend} = handleMessage(data)
    io.to(room).emit('tags_update', dataToSend)
  })
});

//start our server

server.listen(port)
