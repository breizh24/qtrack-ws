FROM node:10


ADD . / /socketio/
WORKDIR /socketio/

RUN npm ci && npm update

EXPOSE 3000

CMD ["npm", "start"]
